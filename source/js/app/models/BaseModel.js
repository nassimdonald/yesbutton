define(['backbone', 'models/BaseModel' ],
    function( Backbone, BaseModel){
		return Backbone.Model.extend({
			defaults: {
				active: false,
				focus: false
			},
	        activate: function() {
	            this.set('active', true);
	        },
	        deactivate: function() {
	            this.set('active', false);
	        }
	    });

	});