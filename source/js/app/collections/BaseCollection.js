define([ 'underscore', 'backbone', 'models/BaseModel'],
    function( _, Backbone, BaseModel){

        return Backbone.Collection.extend({
            model: BaseModel,
            nextElement: function() {
                var index = this.indexOf(this.findWhere({active: true}));
                if ((index + 1) === this.length) {
                    return null;
                }
                return this.at(index + 1);
            },
            previousElement: function() {
                var index = this.indexOf(this.findWhere({active: true}));
                if (index === 0 ) {
                    return null;
                }
                return this.at(index - 1);
            },
            nextFocus: function(loop) {
                var focused = this.getFocused(),
                    index = focused ? this.indexOf(focused) : -1;
                if ((index + 1) > (this.length - 1)) {
                    return loop ? this.first() : focused; 
                } else {
                    return this.at(index + 1);
                }
            },
            previousFocus: function(loop) {
                var focused = this.getFocused(),
                    index = focused ? this.indexOf(focused) : this.length;
                if ((index - 1) < 0) {
                    return loop ? this.last() : focused;
                } else {
                    return this.at(index - 1);
                }
            },
            getFocused: function() {
                return this.findWhere({ focus: true });
            },
            getActive: function() {
                return this.findWhere({ active: true });
            },
            unsetActive: function(active) {
                var current = _.without(this.where({ active: true }), active);
                if (current.length) {
                    _.each(current, function(model) {
                        model.set('active', false);
                    });
                }
            }
        });
    });