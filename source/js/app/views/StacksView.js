define( [ 'jquery', 'marionette', 'velocity', 'config/vent', 'views/FooterView', 'text!templates/stacks.html', 'text!templates/base-stack.html' ],
    function( $, Marionette, Velocity, vent, FooterView, stacksTemplate, baseStackTemplate) {

        // NOTE: due to velocity bug with lingering transforms, i remove them manually with .css()

        var stackScale = 0.4;
        var stackTransYBack = '-15%';
        var stackTransYFront = '-100%';
        var stackSpeed = 500;
        var stackSwapSpeed = stackSpeed*1.6;
        var stackEasing = [0.10, 0.70, 0.10, 1]; // ~iOS stack easing
        var stackEasingOut = 'easeOutCubic'; // ~iOS stack easing
        var containerScale = 1 * (1/stackScale);

        var BaseStackView = Marionette.LayoutView.extend({
            tagName: 'article',
            className: function() {
                return 'stack stack-' + this.model.id;
            },
            template: _.template(baseStackTemplate),
            regions: {
                content: '.content-container'
            },
            ui: {
                mask: '.stack-mask'
            },
            events: {
                'click @ui.mask': function() {
                    this.model.activate();
                },
                'keypress @ui.mask': function(ev) {
                    // Enter key
                    if (ev.which === 13) {
                        this.model.activate();
                    }
                },
                'focus @ui.mask': function() {
                    this.model.set('focus', true);
                },
                'blur @ui.mask': function() {
                    this.model.set('focus', false);
                }
            },
            modelEvents: {
                'change:active': function(model, active) {
                    if (active) {
                        this.triggerMethod('stack:active');
                    } else {
                        this.triggerMethod('stack:inactive');
                    }
                },
                'change:focus': function(model, focus) {
                    this.$el.toggleClass('focus', focus);
                }
            },
            onShow: function() {
                var detailView = this.model.get('detailView');
                this.showChildView('content', new detailView({ model: this.model }));

                this.dummy = $(this.$el[0].cloneNode(false)).addClass('dummy');
                this.dummy.insertAfter(this.$el);
                this.dummy.hide();
            },
            focusEl: function() {
                this.ui.mask.focus();
            }
        });

        return Marionette.CompositeView.extend({
            id: 'stacks-container',
            sectionId: 'projects',
            inverse: true,
            template: _.template(stacksTemplate),
            childViewContainer: '#stacks',
            childView: BaseStackView,
            regions: {
                footer: '#stacks-footer'
            },
            ui: {
                stacksHeader: '#stacks-header',
                logo: '#stacks-header .logo',
                container: '#stacks'
            },
            events: {
                'click @ui.logo': function(ev) {
                    ev.preventDefault();
                    vent.trigger('intro:reset');
                }
            },
            collectionEvents: {
                'change:focus': function(model, focus) {
                    var focused = _.without(this.collection.where({ focus: true }), model)[0];
                    if (focus && focused) {
                        focused.set('focus', false);
                    }
                }
            },
            initialize: function() {
                this.regionManager = new Marionette.RegionManager();
                this.regionManager.addRegions(this.regions);
            },
            onShow: function() {
                var that = this;
                var activeStackModel = this.collection.getActive();

                this.regionManager.get('footer').show(new FooterView());
                this.ui.container.append(this.regionManager.get('footer').$el);

                this.listenTo(this, 'show', function() {

                    if (activeStackModel) {
                        vent.trigger('stack:show', activeStackModel);
                        this.showStack(this.children.findByModel(activeStackModel));
                        this.resizeHeros();
                    }
                });

                $(window).on('resize.hero-height', function() {
                    that.resizeHeros();
                });
            },
            onBeforeDestroy: function() {
                this.regionManager.destroy();
                this.regionManager = null;
                this.stackActive = false;
                this.stacksAnimating = false;
                $(window).off('resize.hero-height');
                vent.trigger('stacks:close');
            },
            resizeHeros: function() {
                var height = this.$el.height();
                this.collection.each(function(model) {
                    if (model.get('hero-height') === height) {
                        model.trigger('change:hero-height', model, height);
                    } else {
                        model.set('hero-height', height);
                    }
                });
            },
            up: function(ev) {
                var previous = this.collection.previousFocus(),
                    child;
                if (previous === this.collection.getFocused()) {
                    return;
                } else {
                    ev.preventDefault();
                    previous.set('focus', true);
                    child = this.children.findByModel(previous);
                    this.$el
                        .stop()
                        .animate({
                            scrollTop: child.$el.position().top
                        }, 200, 'linear', function() {
                            child.focusEl();
                        });
                }
            },
            down: function(ev) {
                var next = this.collection.nextFocus(),
                    child;
                if (next === this.collection.getFocused()) {
                    return;
                } else {
                    ev.preventDefault();
                    next.set('focus', true);
                    child = this.children.findByModel(next);
                    this.$el
                        .stop()
                        .animate({
                            scrollTop: child.$el.position().top
                        }, 200, 'linear', function() {
                            child.focusEl();
                        });
                }
            },
            activateStackByRoute: function(route) {
                var model = this.collection.findWhere({ route: route });
                if (model) {
                    model.activate();
                } else {
                    vent.trigger('notfound:show');
                }
            },
            hideActiveStack: function() {
                if (this.activeStack && this.stackActive) {
                    this.activeStack.model.deactivate();
                }
            },
            showActiveStack: function() {
                if (this.activeStack && !this.stackActive) {
                    this.activeStack.model.activate();
                }
            },
            onChildviewStackActive: function(view) {
                if (this.stacksAnimating) {
                    return;
                }

                if (this.stackActive) {
                    this.swapStacks(view);
                } else if (!this.activeStack && this.ui.container.is(':hidden')) {
                    this.showStack(view);
                } else {
                    this.scaleInStack(view);
                }

                vent.trigger('stack:show', view.model);
            },
            onChildviewStackInactive: function(view) {
                if (this.stacksAnimating) {
                    return;
                }

                this.scaleOutStack(view);
            },
            onStacksAnimationStart: function(view) {
                this.stackActive = true;
                this.activeStack = view || this.activeStack;
                this.stacksAnimating = true;
                this.triggerMethod('stacks:animating');
            },
            onStacksAnimationEnd: function(active) {
                this.stackActive = active || false;
                this.stacksAnimating = false;
                this.triggerMethod('stacks:animated');
                if (!active && this.activeStack) {
                    // Strange flash?
                    this.activeStack.model.set('focus', true);
                }
            },
            updateBackground: function(active) {
                var model = active || this.collection.getActive(),
                    bg = model.get('background') || '';
                this.triggerMethod('stacksbg:update', bg);
            },
            showStacks: function() {
                var that = this;

                if (this.stacksAnimating) {
                    return;
                }

                vent.trigger('stacks:visible');

                if (this.stackActive) {
                    this.hideActiveStack();
                    this.resizeHeros();
                } else if (this.ui.container.is(':hidden')) {
                    this.ui.container.show();
                    this.resizeHeros();
                    this.ui.container.hide();

                    this.ui.container.velocity('fadeIn', {
                        duration: stackSpeed,
                        easing: stackEasingOut
                    });
                }
            },
            showStack: function(view) {
                var that = this;
                var el = view.$el;

                this.onStacksAnimationStart(view);

                view.dummy.show();

                $.Velocity.hook(el.find('.stack-inner'), 'scale', 1);
                $.Velocity.hook(el, 'position', '');
                $.Velocity.hook(el, 'height', '100%');
                $.Velocity.hook(el, 'top', 0);

                el.addClass('selecting active');

                el.insertAfter(this.$el);

                this.updateBackground();

                el.velocity('fadeIn', {
                    duration: stackSpeed,
                    easing: stackEasing,
                    complete: function() {
                        that.onStacksAnimationEnd(true);
                    }
                });
            },
            swapStacks: function(view) {
                var that = this;
                var oldView = this.activeStack;
                var direction = this.collection.indexOf(oldView.model) < this.collection.indexOf(view.model) ? 'next' : 'prev';

                this.onStacksAnimationStart(view);
                this.slideInStack(view, direction);
                this.slideOutStack(oldView, direction);
            },
            slideInStack: function(view, direction) {
                var that = this;
                var el = view.$el;
                var inner = el.find('.stack-inner');
                var scrollTop = el.scrollTop();
                var transY, startScale;

                if (direction === 'prev') {
                    transY = stackTransYBack;
                    startScale = stackScale;
                } else {
                    transY = stackTransYFront;
                    startScale = 1/stackScale;
                    el.addClass('top-stack');
                }

                el.addClass('selecting');

                view.dummy.show();

                $.Velocity.hook(inner, 'scale', startScale);
                $.Velocity.hook(inner, 'opacity', 0);
                $.Velocity.hook(el, 'position', 'absolute');
                $.Velocity.hook(el, 'height', '100%');
                $.Velocity.hook(el, 'top', 0);

                el.insertAfter(this.$el);

                el.scrollTop(scrollTop*(1/stackScale));

                that.resizeHeros();

                this.updateBackground(view.model);

                inner
                    .css({
                        'transformOrigin': '50% ' + transY,
                        'overflow': 'hidden',
                        'height': '100%',
                        'position': 'absolute'
                    })
                    .velocity({
                        scale: 1,
                        opacity: 1
                    }, {
                        duration: stackSwapSpeed*0.75,
                        easing: stackEasing,
                        complete: function() {

                            inner.css({
                                'transformOrigin': '',
                                'overflow': '',
                                'height': '',
                                'position': ''
                            });
                            $.Velocity.hook(inner, 'opacity', '');

                            $.Velocity.hook(el, 'position', '');
                            $.Velocity.hook(el, 'height', '');
                            $.Velocity.hook(el, 'top', '');

                            el
                                .addClass('active')
                                .removeClass('top-stack');

                            $(window).scrollTop(scrollTop/startScale);
                        }
                    });
            },
            slideOutStack: function(view, direction, callback) {
                var that = this;
                var el = view.$el;
                var inner = el.find('.stack-inner');
                var scrollTop = $(window).scrollTop();
                var transY, scaleTo;

                if (direction === 'prev') {
                    transY = stackTransYFront;
                    scaleTo = 1/stackScale;
                    el.addClass('top-stack');
                } else {
                    transY = stackTransYBack;
                    scaleTo = stackScale;
                }

                el.removeClass('active');
                $.Velocity.hook(el, 'position', 'absolute');
                $.Velocity.hook(el, 'height', '100%');
                $.Velocity.hook(el, 'top', '0');

                inner
                    .css({
                        'transformOrigin': '50% ' + transY,
                        'overflow': 'hidden',
                        'height': '100%',
                        'position': 'absolute'
                    })
                    .velocity({
                        scale: scaleTo,
                        opacity: 0
                    }, {
                        duration: stackSwapSpeed,
                        easing: stackEasingOut,
                        begin: function() {
                            inner.scrollTop(scrollTop);
                        },
                        complete: function() {

                            el.insertBefore(view.dummy);

                            view.dummy.hide();

                            inner.css({
                                'transformOrigin': '',
                                'overflow': '',
                                'height': '',
                                'position': ''
                            });
                            $.Velocity.hook(inner, 'scale', stackScale);
                            $.Velocity.hook(inner, 'opacity', '');

                            $.Velocity.hook(el, 'position', '');
                            $.Velocity.hook(el, 'height', '');
                            $.Velocity.hook(el, 'top', '');

                            el.removeClass('selecting top-stack');

                            // el.scrollTop(scrollTop*stackScale);

                            view.model.deactivate();

                            that.onStacksAnimationEnd(true);
                        }
                    });
            },
            scaleInStack: function(view) {
                var that = this;
                var el = view.$el;
                var container = this.ui.container;
                var stackOffset = el.offset().top;
                var scrollTop = el.scrollTop();
                var stacksTransY;

                this.onStacksAnimationStart(view);

                el.addClass('selecting');

                view.dummy.show();

                el.insertAfter(this.$el);

                stacksTransY = view.dummy.position().top + (stackOffset*stackScale);

                $.Velocity.hook(el, 'position', 'absolute');
                $.Velocity.hook(el, 'top', stackOffset + 'px');
                el.scrollTop(scrollTop);

                this.updateBackground();

                container
                    .css('transformOrigin', '50% ' + stacksTransY + 'px')
                    .velocity({
                        opacity: 0,
                        scale: containerScale
                    }, {
                        display: 'none',
                        easing: stackEasing,
                        duration: stackSpeed,
                        complete: function() {
                            $.Velocity.hook(container, 'scale', 1);
                        }
                    });

                el.find('.stack-inner')
                    .velocity({
                        scale: [1, stackScale],
                    }, {
                        duration: stackSpeed,
                        easing: stackEasing
                    })
                    .velocity('scroll', {
                      container: el,
                      offset: scrollTop/stackScale,
                      duration: stackSpeed,
                      easing: stackEasing,
                      queue: false,
                    });

                el
                    .velocity({
                        height: '100%',
                        top: 0
                    }, {
                        duration: stackSpeed,
                        easing: stackEasing,
                        complete: function() {
                            $.Velocity.hook(el, 'position', '');

                            el.addClass('active');

                            $(window).scrollTop(scrollTop/stackScale);

                            that.onStacksAnimationEnd(true);
                        }
                    });
            },
            scaleOutStack: function(view) {
                var that = this;
                var el = view.$el;
                var inner = el.find('.stack-inner');
                var container = this.ui.container;
                var dummyOffset;
                var stackOffset = el.offset().top;
                var scrollTop = $(window).scrollTop();
                var stacksTransY;

                this.onStacksAnimationStart(view);

                $.Velocity.hook(container, 'display', 'block');

                view.dummy.get(0).scrollIntoView(view.model.collection.first() !== view.model);

                el.removeClass('active');
                $.Velocity.hook(el, 'position', 'absolute');
                $.Velocity.hook(el, 'height', '100%');
                $.Velocity.hook(inner, 'scale', 1);
                el.scrollTop(scrollTop);

                dummyOffset = view.dummy.offset().top;

                stacksTransY = view.dummy.position().top + (dummyOffset*stackScale);

                $.Velocity.hook(container, 'scale', containerScale);

                container
                    .css('transformOrigin', '50% ' + stacksTransY + 'px')
                    .velocity({
                        opacity: 1,
                        scale: 1
                    }, {
                        duration: stackSpeed,
                        easing: stackEasing
                    });

                inner
                    .velocity({
                        scale: stackScale
                    }, {
                        duration: stackSpeed,
                        easing: stackEasing
                    })
                    .velocity('scroll', {
                      container: el,
                      offset: scrollTop * stackScale,
                      duration: stackSpeed,
                      easing: stackEasing,
                      queue: false
                    });

                el
                    .velocity({
                        top: dummyOffset + 'px',
                        height: (stackScale * 100) + '%'
                    }, {
                        duration: stackSpeed,
                        easing: stackEasing,
                        complete: function() {

                            el.insertBefore(view.dummy);

                            view.dummy.hide();

                            $.Velocity.hook(el, 'position', '');
                            $.Velocity.hook(el, 'top', '');
                            $.Velocity.hook(el, 'height', '');

                            el
                                .removeClass('selecting')
                                .scrollTop(scrollTop*stackScale);

                            that.onStacksAnimationEnd();
                        }
                    });
            }
        });
    });