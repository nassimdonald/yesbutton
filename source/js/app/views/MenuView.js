define([ 'marionette', 'velocity', 'config/vent', 'text!templates/menu.html'],
    function ( Marionette, Velocity, vent, menuTemplate) {

        var NavItem = Marionette.LayoutView.extend({
            tagName: 'li',
            template: _.template('<a href="/<%=id%>"><span><%=name%></span></a>'),
            events: {
                'click a': function(ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                    if (!this.model.get('active')) {
                        this.model.set('active', true);
                    }
                    this.triggerMethod('show:section');
                },
                'focus a': function() {
                    this.model.set('focus', true);
                },
                'blur a': function() {
                    this.model.set('focus', false);
                }
            },
            modelEvents: {
                'change:active': function(model, active) {
                    this.$el.toggleClass('active', active);
                },
                'change:focus': function(model, focus) {
                    if (focus && !this.$el.is(':focus')) {
                        this.$el.find('a').focus();
                    }
                }
            },
            onShow: function() {
                $.Velocity.hook(this.$el, 'opacity', 0);
                $.Velocity.hook(this.$el, 'scale', 1.5);
            },
            onMenuShow: function(index) {
                var delay = index * 80;
                var transOrigY = ((this.model.collection.length / 2) * 100) - (index * 100) + '%';

                // Transform relative to list Y center
                $.Velocity.hook(this.$el, 'transformOrigin', '50% ' + transOrigY);

                this.$el
                    .velocity('stop')
                    .velocity({
                        opacity: 1,
                        scale: 1
                    }, {
                        delay: delay,
                        duration: 600,
                        easing: [0.10, 0.70, 0.10, 1]
                    });
            },
            onMenuHide: function() {
                this.$el
                    .velocity('stop')
                    .velocity('reverse');
            }
        });

        return Marionette.CompositeView.extend({
            className: 'menu-inner',
            template: _.template(menuTemplate),
            childView: NavItem,
            childViewContainer: 'nav > ul',
            events: {
            	'click': function() {
            		this.triggerMethod('menu:hide');
            	},
                'click .logo': function(ev) {
                    ev.preventDefault();
                    vent.trigger('intro:reset');
                }
            },
            collectionEvents: {
                'change:active': function(model, active) {
                    if (active) {
                        this.collection.unsetActive(model);
                    }
                }
            },
            onBeforeDestroy: function() {
            	this.onMenuHide(true);
            },
            onChildviewShowSection: function(view) {
                var app = require('App');
                vent.trigger(view.model.id + ':show');
                this.triggerMethod('menu:hide');
                app.appRouter.navigate('/' + view.model.id);
            },
            onMenuShow: function(sectionId) {
            	var that = this;
                var app = require('App');

                if (this.collection.get(sectionId)) {
                    this.collection.get(sectionId).set('active', true);
                }

            	this.listenTo(app.appRouter, 'route', function() {
                    this.triggerMethod('menu:hide')
                });

            	this.$el
                    .velocity('stop')
                    .velocity('fadeIn', 200);

                this.children.each(function(view, index) {
                    view.onMenuShow(index);
                });
            },
            onMenuHide: function(suppressAnimation) {
                var app = require('App'),
                    focus = this.collection.findWhere({focus: true});

            	this.stopListening(app.appRouter, 'route');

                if (focus) {
                    focus.set('focus', false);
                }

            	if (suppressAnimation === true) {
            		return;
            	}

                this.children.each(function(view) {
                    view.onMenuHide();
                });

            	this.$el
                    .velocity('stop')
                    .velocity('fadeOut', 200);
            },
            up: function(ev) {
                ev.preventDefault();
                this.collection.previousFocus(true).set('focus', true);
            },
            down: function(ev) {
                ev.preventDefault();
                this.collection.nextFocus(true).set('focus', true);
            }
        });
    });