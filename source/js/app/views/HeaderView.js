define([ 'jquery', 'marionette', 'velocity', 'config/vent', 'text!templates/header.html'],
    function ( $, Marionette, Velocity, vent, headerTemplate) {

        return Marionette.LayoutView.extend({
            className: 'header-inner',
            template: _.template(headerTemplate),
            ui: {
                multiMenu: '.multi-menu-icon',
                stacksNav: '.stacks-nav',
                prev: '.prev-stack',
                next: '.next-stack'
            },
            events: {
                'click @ui.multiMenu': function(ev) {
                    ev.preventDefault();
                    if (this.menuActive) {
                        this.triggerMethod('menu:hide');
                    } else if (this.activeStack) {
                        this.triggerMethod('stacks:show');
                    } else {
                        this.triggerMethod('menu:show');
                    }
                },
                'click @ui.next': function(ev) {
                    ev.preventDefault();
                    this.triggerMethod('stack:next');
                },
                'click @ui.prev': function(ev) {
                    ev.preventDefault();
                    this.triggerMethod('stack:prev');
                }
            },
            initialize: function() {
                var that = this;

                this.listenTo(vent, 'stack:show', function(active) {
                    var index = active.collection.indexOf(active);
                    var next = active.collection.at(index + 1);
                    var prev = active.collection.at(index - 1);

                    if (index < 2) {
                        this.ui.prev.removeAttr('href');
                    } else {
                        this.ui.prev.attr('href', prev.get('route'));
                    }

                    if (!next) {
                        this.ui.next.removeAttr('href');
                    } else {
                        this.ui.next.attr('href', next.get('route'));
                    }

                    if (!active.get('isProject')) {
                        this.ui.stacksNav.velocity('fadeOut', 200);
                    } else {
                        this.ui.stacksNav.velocity('fadeIn', 200);
                    }

                    this.ui.multiMenu.addClass('back');

                    this.onMainShow(active.get('inverse'));

                    this.activeStack = active;
                });
                this.listenTo(vent, 'stacks:visible', this.onStacksReset);
                this.listenTo(vent, 'stacks:close', this.onStacksReset);

                this.menuActive = false;
            },
            onShow: function() {
                // Hiding fixed header - credit to Marius Craciunoiu:
                // https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c#.vqnj0gpcx
                var that = this;
                var didScroll;
                var lastScrollTop = 0;
                var delta = 5;
                var navbarHeight = 90;

                $(window).on('scroll.fixed-nav', function(event){
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);

                function hasScrolled() {
                    var st = $(this).scrollTop();

                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                        return;

                    // If they scrolled down and are past the navbar, add class .nav-up.
                    // This is necessary so you never see what is "behind" the navbar.
                    if (st < $(window).height() - navbarHeight) {
                        that.$el.removeClass('nav-down nav-up');
                    } else if (st > lastScrollTop && st > navbarHeight){
                        // Scroll Down
                        that.$el.removeClass('nav-down').addClass('nav-up');
                    } else {
                        // Scroll Up
                        if(st + $(window).height() < $(document).height()) {
                            that.$el.removeClass('nav-up').addClass('nav-down');
                        }
                    }

                    lastScrollTop = st;
                }
            },
            onBeforeDestroy: function() {
                $(window).off('keydown.stack-controls');
            },
            onMenuShow: function() {
                this.ui.multiMenu
                    .addClass('close')
                    .removeClass('back');
                this.menuActive = true;

                if (this.activeStack) {
                    this.ui.stacksNav.velocity('fadeOut', 200);
                }
            },
            onBeforeDestroy: function() {
                $(window).off('scroll.fixed-nav');
            },
            onMenuHide: function() {
                this.ui.multiMenu.removeClass('close');
                this.menuActive = false;

                if (this.activeStack) {
                    this.ui.multiMenu.addClass('back');

                    if (this.activeStack.get('isProject')) {
                        this.ui.stacksNav.velocity('fadeIn', 200);
                    }
                }
            },
            onStacksReset: function() {
                this.ui.multiMenu.removeClass('back');
                this.ui.stacksNav.velocity('fadeOut', 200);
                this.onMainShow(true);
                this.activeStack = false;
            },
            onMainShow: function(inverse) {
                this.$el.toggleClass('inverse', inverse);
            }
        });
    });