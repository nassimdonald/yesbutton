define([ 'marionette', 'velocity', 'views/FooterView' ],
    function ( Marionette, Velocity, FooterView ) {

        return Marionette.LayoutView.extend({
            regions: {
                footer: '.footer'
            },
            onBeforeShow: function() {
                this.showChildView('footer', new FooterView());
            },
            onShow: function() {
                var that = this;

                this.onResize();

                this.$el.velocity('fadeIn', {
                    duration: 500,
                    easing: 'easeOutCubic'
                });

                $(window).on('resize.page-height', function() {
                    that.onResize();
                });
            },
            onBeforeDestroy: function() {
                $(window).off('resize.page-height');
            },
            onResize: function() {
                var height = $(window).height() - this.footer.$el.height();
                this.$el.find('.container:first').css('minHeight', height);
            }
        });
    });