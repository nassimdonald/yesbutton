define([ 'jquery', 'marionette', 'fastclick', 'velocity', 'config/vent', 'views/HeaderView', 'views/MenuView', 'views/StacksView', 'views/AboutView', 'views/ContactView', 'views/NotFoundView', 'models/ContactModel', 'collections/BaseCollection', 'collections/Stacks'],
    function ( $, Marionette, FastClick, Velocity, vent, HeaderView, MenuView, StacksView, AboutView, ContactView, NotFoundView, ContactModel, BaseCollection, stacks) {

        var menuItems = [{
                id: 'projects',
                name: 'Projects'
            }, {
            //     id: 'about',
            //     name: 'About Yes Button'
            // }, {
                id: 'contact',
                name: 'Get in touch'
            }];

        //A LayoutView is a hybrid of an ItemView and a collection of Region objects. They are ideal for rendering application layouts with multiple sub-regions managed by specified region managers.
        return Marionette.LayoutView.extend({
            el: 'body',
            regions: {
                header:"#global-header",
                menu: '#global-menu',
                main:"#main"
            },
            childEvents: {
                'menu:show': 'onMenuShow',
                'menu:hide': 'onMenuHide',
                'stacks:animating': function() {
                    this.$el.addClass('stacks-animating');
                },
                'stacks:animated': function() {
                    this.$el.removeClass('stacks-animating');
                },
                'stacks:show': function() {
                    if (!this.introduction) {
                        this.showStacks();
                    }
                },
                'stack:next': 'onStackNext',
                'stack:prev': 'onStackPrev',
                'stacksbg:update': function(view, bg) {
                    // Background must not be in scrollable container
                    // to properly use fixed positioning in iOS.
                    this.$el.find('#stacks-background').css('backgroundImage', 'url("' + bg + '")');
                }
            },
            initialize: function() {
                this.listenTo(vent, 'stack:show', function() {
                    this.$el.removeClass('stacks-visible');
                });
                this.listenTo(vent, 'stacks:visible', function() {
                    this.$el.addClass('stacks-visible');
                });
                this.listenTo(vent, 'intro:show', function(toggle) {
                    this.introduction = toggle;
                    this.$el.toggleClass('introduction', toggle);
                });
                this.listenTo(vent, 'intro:reset', function() {
                    this.onStackRoute("");
                });
            },
            onAppShow: function() {
                var stacksBackground = this.$el.find('#stacks-background');

                FastClick.attach(this.$el[0]);

                this.showChildView('header', new HeaderView());
                this.listenTo(this.main, 'show', function(view) {
                    if (view.inverse) {
                        this.header.currentView.onMainShow(view.inverse);
                    }
                });

                // Fix for jumpy fixed background in iOS
                // Credit: http://stackoverflow.com/questions/24944925/background-image-jumps-when-address-bar-hides-ios-android-mobile-chrome
                $(window).on('resize.resizebg', resizeBackground);
                function resizeBackground() {
                    stacksBackground.height($(window).height() + 70);
                }
                resizeBackground();
            },
            toggleMenu: function() {
                if (this.introduction) {
                    return;
                }

                if (this.$el.hasClass('menu-active')) {
                    this.onMenuHide();
                } else {
                    this.onMenuShow();
                    this.header.currentView.onMenuShow();
                }
            },
            onMenuShow: function() {
                var menuCollection;

                this.$el.addClass('menu-active');

                if (!this.menu.currentView) {
                    menuCollection = new BaseCollection(menuItems);
                    this.showChildView('menu', new MenuView({
                        collection: menuCollection
                    }));
                }

                this.menu.currentView.onMenuShow(this.main.currentView.sectionId);
            },
            onMenuHide: function(view) {
                this.$el.removeClass('menu-active');

                if (!view || view === this.menu.currentView) {
                    this.header.currentView.onMenuHide();
                }
                if (!view || view === this.header.currentView) {
                    this.menu.currentView.onMenuHide();
                }
            },
            showStacksView: function(route) {
                var stacksCollection, model;

                if (this.stacksView && this.main.currentView === this.stacksView) {
                    return;
                }

                stacksCollection = new BaseCollection(stacks);

                this.stacksView = new StacksView({
                    collection: stacksCollection
                });

                if (route || route === "") {
                    model = this.stacksView.collection.findWhere({ route: route });

                    if (!model) {
                        vent.trigger('notfound:show');
                        return;
                    }

                    model.activate();
                }

                this.showChildView('main', this.stacksView);
            },
            showStacks: function() {
                this.showStacksView();
                if (!this.stacksView.stacksAnimating) {
                    this.stacksView.showStacks();
                }
            },
            onStackRoute: function(route) {
                if (this.stacksView && this.main.currentView === this.stacksView) {
                    this.stacksView.activateStackByRoute(route);
                } else {
                    this.showStacksView(route);
                }
            },
            onStackNext: function() {
                var next = this.stacksView && this.stacksView.collection.nextElement();

                if (next && next.collection.getActive() !== next.collection.first()
                    && !this.stacksView.stacksAnimating && this.stacksView.stackActive
                    && !this.introduction && !this.$el.hasClass('menu-active')) {
                    next.activate();
                }
            },
            onStackPrev: function() {
                var prev = this.stacksView && this.stacksView.collection.previousElement();

                if (prev && prev.collection.first() !== prev
                    && !this.stacksView.stacksAnimating && this.stacksView.stackActive
                    && !this.introduction && !this.$el.hasClass('menu-active')) {
                    prev.activate();
                }
            },
            showAbout: function() {
                var aboutView = new AboutView();

                this.showChildView('main', aboutView);
            },
            showContact: function() {
                var contactModel = new ContactModel(),
                    contactView = new ContactView({
                        model: contactModel
                    });

                this.showChildView('main', contactView);
            },
            showNotFound: function() {
                var notFoundView = new NotFoundView();
                this.showChildView('main', notFoundView);
            },
            onEscapeKey: function() {
                if (this.$el.hasClass('menu-active')){
                    this.onMenuHide();
                } else if (this.main.currentView === this.stacksView && this.stacksView.stackActive) {
                    this.showStacks();
                }
            },
            onUpKey: function(ev) {
                if (this.$el.hasClass('menu-active')) {
                    this.menu.currentView.up(ev);
                } else if (this.main.currentView === this.stacksView && !this.stacksView.stackActive) {
                    this.stacksView.up(ev);
                }
            },
            onDownKey: function(ev) {
                if (this.$el.hasClass('menu-active')) {
                    this.menu.currentView.down(ev);
                } else if (this.main.currentView === this.stacksView && !this.stacksView.stackActive) {
                    this.stacksView.down(ev);
                }
            }
        });
    });